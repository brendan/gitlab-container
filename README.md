
# GitLab Container
A simple Docker container that just prints Tanuki & GitLab

```bash
docker build -t contaier-gitlab .
docker run -e container-gitlab
```

# Inspired by
## Containerick
https://github.com/chipironcin/containerick
